import random
history=dict()
player_score=0
computer_score=0
def play():
    global player_score
    global computer_score
    ls=["rock","paper","scissor"]
    for i in range(1,11):
        hum=str(input("Enter Rock ,Paper or Scissor"))
        human=hum.lower()
        computer=random.choice(ls)
        history[i]=(human,computer)
        show(i)
    print(history)
    print("Play is over")
    print('*'*10,'Score','*'*10)
    if player_score>computer_score:
        print('player won')
    elif player_score<computer_score:
        print('Computer won')
    else:
        print('It is a draw')
    print('Score is')
    print('Player:',player_score,' '*6,'Computer score:',computer_score)

def show(n):
    global player_score
    global computer_score
    human,computer=history[n]
    if human=='rock':
        if computer=='paper':
            print('Player Choice ',human)
            print('Computer Choice ',computer)
            print('Computer Won Round',n)
            computer_score+=1
        elif computer=='scissor':
            print('Player Choice ',human)
            print('Computer Choice ',computer)
            print('Player Won Round',n)
            player_score+=1
        else:
            print('Player Choice ',human)
            print('Computer Choice ',computer)
            print('Round',n,'is draw')
    if human=='paper':
        if computer=='rock':
            print('Player Choice ',human)
            print('Computer Choice ',computer)
            print('Player Won Round',n)
            player_score+=1
        elif computer=='scissor':
            print('Player Choice ',human)
            print('Computer Choice ',computer)
            print('Computer Won Round',n)
            computer_score+=1
        else:
            print('Player Choice ',human)
            print('Computer Choice ',computer)
            print('Round',n,'is draw')
    if human=='scissor':
        if computer=='paper':
            print('Player Choice ',human)
            print('Computer Choice ',computer)
            print('Player Won Round-',n)
            player_score+=1
        elif computer=='rock':
            print('Player Choice ',human)
            print('Computer Choice ',computer)
            print('Computer won Round',n)
            computer_score+=1
        else:
            print('Player Choice ',human)
            print('Computer Choice ',computer)
            print('Round',n,'is draw')
def main():
    play()
    while True:
        n=int(input("Enter the name of the round"))
        show(n)

main()
